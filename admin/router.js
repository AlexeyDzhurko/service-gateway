import Vue from 'vue'
import Router from 'vue-router'

import Index from './pages/index'
import SignIn from './pages/autorization/SignIn'
import Users from './pages/user/UsersList'
import ManageUsers from './pages/user/ManageUsers'
import ServicesList from './pages/services/ServicesList'
import ManageService from './pages/services/ManageService'

Vue.use(Router)

/**
 * @returns {Router}
 */
export function createRouter() {
  return new Router({
    mode: 'history',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Index
      },
      {
        name: 'sign-in',
        path: '/sign-in',
        component: SignIn
      },
      {
        name: 'users',
        path: '/users',
        component: Users
      },
      {
        name: 'manage-users',
        path: '/users/:id?',
        component: ManageUsers
      },
      {
        name: 'services',
        path: '/services/:id?',
        component: ServicesList
      },
      {
        name: 'manage-service',
        path: '/services/:id?',
        component: ManageService
      },
    ]
  })
}
