/**
 * @returns {{isAuthorized: boolean}}
 */
export const state = () => ({
  isAuthorized: false,
});

/**
 * @type {{isAuthorized(*): boolean}}
 */
export const getters = {
  isAuthorized(state) {
    return state.isAuthorized;
  },
};

/**
 * @type {{SET_IS_AUTHORIZED(*, *): void}}
 */
export const mutations = {
  SET_IS_AUTHORIZED(state, value) {
    state.isAuthorized = value;
  },
};

/**
 * @type {{
 * setIsAuthorized: actions.setIsAuthorized,
 * restorePassword: ((function({commit: *}, *=): Promise<void>)|*),
 * login: ((function({commit: *}, *=): Promise<any|undefined>)|*),
 * confirmEmail: (function({commit: *}, *=): any),
 * register: ((function({commit: *}, *=): Promise<void>)|*)}}
 */
export const actions = {
  setIsAuthorized: function({commit}, value) {
    commit('SET_IS_AUTHORIZED', value);
  },
  login: async function({commit}, params = {}) {
    let res = await this.$axios.post('/auth/sign-in', params);

    if (res.data) {
      const data = res.data;
      localStorage.setItem(
        'token',
        `${data.data.attributes.tokenType} ${data.data.attributes.accessToken}`,
      );
      commit('SET_IS_AUTHORIZED', true);

      return data;
    }
  },
  register: async function({commit}, params = {}) {
    let {data} = await this.$axios.post('/auth/register', params);
  },
  restorePassword: async function({commit}, params = {}) {
    let {data} = await this.$axios.post('/auth/restore-password', params);
  },
  restoreToken: async function({commit}, params = {}) {
    let res = await this.$axios.post('/auth/refresh-token', params);
    if (res.data) {
      const data = res.data;
      localStorage.setItem(
          'token',
          `${data.data.attributes.tokenType} ${data.data.attributes.accessToken}`,
      );
      commit('SET_IS_AUTHORIZED', true);
    }

    return res;
  },
  confirmEmail: async function({commit}, params = {}) {
    let {data} = await this.$axios.post('/auth/confirm-email', params);
    localStorage.setItem('token', `${data.tokenType} ${data.accessToken}`);
    commit('SET_IS_AUTHORIZED', true);

    return data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
