import Service from "../dto/entities/Service";

/**
 * @returns {{
 * servicesList: *[],
 * service: null
 * }}
 */
export const state = () => ({
    service: null,
    servicesList: []
});

/**
 *
 * @type {{
 * getservice(*): null,
 * getservicesList(*): []
 * }}
 */
export const getters = {
    getService(state) {
        return state.service;
    },
    getServicesList(state) {
        return state.servicesList;
    }
};

/**
 * @type {{
 * SET_SERVICES_LIST(*, *): void,
 * SET_SERVICE(*, *): void
 * }}
 */
export const mutations = {
    SET_SERVICE(state, value) {
        state.service = value
    },
    SET_SERVICES_LIST(state, value) {
        state.servicesList = value
    },
};

/**
 * @type {{
 * fetchServicesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchService: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchServicesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/services', params);

        const list = data.data.map((serviceData) => {
            return new Service(serviceData)
        });

        commit('SET_SERVICES_LIST', list)
    },
    fetchService: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/services/${params.id}`, params);
            commit('SET_SERVICE', new Service(data))
        } else {
            commit('SET_SERVICE', new Service(null))
        }
    },
    registerService: async function ({commit}, params = {}) {
        await this.$axios.post(`/services`, params).then((res) => {
            if (res) {
                commit('SET_SERVICE', new Service(res.data))
            }
        });
    },
    updateService: async function ({commit}, params = {}) {
        await this.$axios.put(`/services/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_SERVICE', new Service(res.data))
            }
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
