/**
 * @returns {{currentUser: {}}}
 */
import User from '../dto/entities/User';
import UserPermissions from "../dto/entities/UserPermissions";

export const state = () => ({
    currentUser: null,
    user: null,
    usersList: [],
    userPermissionsList: []
});

/**
 * @type {{getCurrentUser(*): []}}
 */
export const getters = {
    getCurrentUser(state) {
        return state.currentUser;
    },
    getUser(state) {
        return state.user;
    },
    getUserPermissions(state) {
        return state.userPermissionsList;
    },
    getUsersList(state) {
        return state.usersList;
    }
};

/**
 * @type {{SET_CURRENT_USER(*, *): void}}
 */
export const mutations = {
    SET_CURRENT_USER(state, value) {
        state.currentUser = value
    },
    SET_USER(state, value) {
        state.user = value
    },
    SET_USER_PERMISSIONS_LIST(state, value) {
        state.userPermissionsList = value
    },
    SET_USERS_LIST(state, value) {
        state.usersList = value
    },
};

/**
 * @type {{
 * fetchUsersList: ((function({commit: *}, *=): Promise<void>)|*),
 * registerUser: ((function({commit: *}, *=): Promise<void>)|*),
 * updateUser: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchUser: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCurrentUser: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchUsersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/users', params);

        const list = data.data.map((userData) => {
            return new User(userData)
        });

        commit('SET_USERS_LIST', list)
    },
    fetchUser: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/users/${params.id}`, params);
            commit('SET_USER', new User(data))
        } else {
            commit('SET_USER', new User({}))
        }
    },
    fetchUserPermissionsList: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/users/${params.id}/permissions`, params);
            const list = data.data.map((data) => {
                return new UserPermissions(data)
            });
            commit('SET_USER_PERMISSIONS_LIST', list)
        } else {
            commit('SET_USER_PERMISSIONS_LIST', []);
        }
    },
    fetchCurrentUser: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get(`/users/${params.id}`, params);
        commit('SET_CURRENT_USER', new User(data))
    },
    registerUser: async function ({commit}, params = {}) {
        await this.$axios.post(`/users`, params).then((res) => {
            if (res) {
                commit('SET_USER', new User(res.data))
            }
        });
    },
    updateUser: async function ({commit}, params = {}) {
        await this.$axios.put(`/users/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_USER', new User(res.data))
            }
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
