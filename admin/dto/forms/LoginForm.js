import {email, required} from 'vuelidate/lib/validators';

export default class LoginForm {

  /**
   * @return <void>
   */
  constructor() {
    this.email = null;
    this.password = null;
  }

  /**
   * @returns {{
   *  password: null,
   *  email: null
   * }}
   */
  model() {
    return {
      email: null,
      password: null,
    };
  }

  /**
   * @returns {{form: {password: {required}, email: {required, email}}}}
   */
  validation() {
    return {
      form: {
        email: {
          required,
          email,
        },
        password: {
          required,
        },
      },
    };
  }

  /**
   * @return <void>
   */
  reset() {
    this.email = null;
    this.password = null;
  }
}
