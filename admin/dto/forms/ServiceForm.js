import {required} from 'vuelidate/lib/validators';

export default class ServiceForm {

    /**
     * @return <void>
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.title = data.title ? data.title : null;
        this.route = data.route ? data.route : null;
        this.link = data.link ? data.link : null;
    }

    /**
     * @returns {{
     *  password: null,
     *  email: null
     * }}
     */
    model() {
        return {
            id: null,
            title: null,
            route: null,
            link: null,
        };
    }

    /**
     * @returns {{form: {password: {required}, email: {required, email}}}}
     */
    validation() {
        return {
            form: {
                title: {
                    required,
                },
                route: {
                    required,
                },
                link: {
                    required,
                },
            },
        };
    }

    /**
     * @return <void>
     */
    reset(data = {}) {
        this.id = data.id ? data.id : null;
        this.title = data.title ? data.title : null;
        this.route = data.route ? data.route : null;
        this.link = data.link ? data.link : null;
    }
}
