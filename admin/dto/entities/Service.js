export default class Service {
  /**
   * @param data
   */
  constructor(data = {}) {
    if (!data) {
      data = {}
    }
    this.id = data.id ? data.id : null;
    this.title = data.attributes && data.attributes.title ?
      data.attributes.title :
      null;
    this.route = data.attributes && data.attributes.route ?
      data.attributes.route :
      null;
    this.link = data.attributes && data.attributes.link ?
      data.attributes.link :
      null;
    this.createdAt = data.attributes && data.attributes.createdAt ?
      data.attributes.createdAt :
      null;
    this.updatedAt = data.attributes && data.attributes.updatedAt ?
      data.attributes.updatedAt :
      null;
  }
}
