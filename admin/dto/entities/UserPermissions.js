import Service from "./Service";
import User from "./User";

export default class UserPermissions {
  /**
   * @param data
   */
  constructor(data = {}) {
    this.access = data.attributes && data.attributes.access;
    this.createdAt = data.attributes && data.attributes.createdAt ?
      data.attributes.createdAt :
      null;
    this.updatedAt = data.attributes && data.attributes.updatedAt ?
      data.attributes.updatedAt :
      null;
    this.service = data.relationships && data.relationships.service
        ? new Service(data.relationships.service)
        : new Service()
    this.user = data.relationships && data.relationships.user
        ? new User(data.relationships.user)
        : new User()
  }
}
