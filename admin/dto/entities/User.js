export default class User {
  /**
   * @param data
   */
  constructor(data = {}) {
    this.id = data.id ? data.id : null;
    this.email = data.attributes && data.attributes.email ?
      data.attributes.email :
      null;
    this.name = data.attributes && data.attributes.name ?
      data.attributes.name :
      null;
    this.isAdmin = data.attributes && data.attributes.isAdmin;
    this.createdAt = data.attributes && data.attributes.createdAt ?
      data.attributes.createdAt :
      null;
    this.updatedAt = data.attributes && data.attributes.updatedAt ?
      data.attributes.updatedAt :
      null;
  }
}
