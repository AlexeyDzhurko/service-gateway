import Vue from 'vue'

import Material from 'vue-material/dist/vue-material'

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(Material)
