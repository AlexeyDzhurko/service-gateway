export default function({ $axios, store, redirect, error }) {
  $axios.onError(async err => {
    const code = parseInt(err.response && err.response.status)

    if (code === 422) {
      const errs = prepareErrors(err.response)
      store.dispatch('validation/setErrors', errs)
    }

    if (code === 404 || code >= 500) {
      store.dispatch('validation/setErrors', {
        message: err.response.data.detail,
        fields: null,
        statusCode: code
      })
    }

    if (code === 403) {
      store.dispatch('validation/setErrors', {
        message: "Forbidden! You can't do this action",
        fields: null,
        statusCode: 403
      })

      dropAuthorization();
    }

    // if (code === 401) {
    //   store.dispatch('validation/setErrors', {
    //     message: err.response.data.detail,
    //     fields: null,
    //     statusCode: 401
    //   })
    //
    //   dropAuthorization();
    // }

    if (code === 440 || code === 401) {
      const originRequest = err.config;

      let response = await store.dispatch('authorization/restoreToken');

      if (response.status === 200) {
        return $axios(originRequest);
      } else {
        localStorage.removeItem('token');
        redirect('/sign-in');
      }
    }

    // return Promise.reject(error)
  })

  $axios.onRequest((config) => {
    store.dispatch('validation/clearErrors')

    const token = typeof window !== 'undefined'
      ? localStorage.getItem('token')
      : null;

    if (token && token.length) {
      config.headers.Authorization = token;
    }
  })

  const dropAuthorization = function() {
    typeof window !== 'undefined'
      ? localStorage.removeItem('token')
      : null;

    store.dispatch('authorization/setIsAuthorized', false)

    redirect('/sign-in');
  }

  const prepareErrors = function (response) {
    const fields = {}
    let msg = ''
    let errData = response.data;

    for (let i in errData) {
      if (errData[i].source) {
        fields[errData[i].source.parameter] = errData[i].detail
      } else {
        msg += errData[i].detail
      }
    }

    const errors = {
      message: msg,
      fields,
      statusCode: response.status
    }
    return errors
  }
}
