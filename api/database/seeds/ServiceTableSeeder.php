<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class ServiceTableSeeder
 */
class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'id' => 1,
                'title' => 'test service',
                'route' => 'test',
                'link' => 'https://test.com',
                'created_at' => Carbon::now()
            ],
            [
                'id' => 2,
                'title' => 'test service 2',
                'route' => 'test-2',
                'link' => 'https://test-2.com',
                'created_at' => Carbon::now()
            ],
        ]);
    }
}
