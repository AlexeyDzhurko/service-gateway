<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class UsersPermissionsTableSeeder
 */
class UsersPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_permissions')->insert([
            [
                'user_id' => 1,
                'service_id' => 1,
                'access' => true
            ],
            [
                'user_id' => 1,
                'service_id' => 2,
                'access' => true
            ],
            [
                'user_id' => 2,
                'service_id' => 1,
                'access' => true
            ],
            [
                'user_id' => 2,
                'service_id' => 2,
                'access' => false
            ],
            [
                'user_id' => 3,
                'service_id' => 1,
                'access' => false
            ],
            [
                'user_id' => 3,
                'service_id' => 2,
                'access' => true
            ],
        ]);
    }
}
