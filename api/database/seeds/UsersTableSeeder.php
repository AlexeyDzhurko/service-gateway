<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'test@test.com',
                'name' => 'admin',
                'is_admin' => true,
                'password' => \Illuminate\Support\Facades\Hash::make('testpass'),
                'created_at' => Carbon::now()
            ],
            [
                'email' => 'user@test.com',
                'name' => 'user',
                'is_admin' => false,
                'password' => \Illuminate\Support\Facades\Hash::make('testpass'),
                'created_at' => Carbon::now()
            ],
            [
                'email' => 'user2@test.com',
                'name' => 'user2',
                'is_admin' => false,
                'password' => \Illuminate\Support\Facades\Hash::make('testpass'),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
