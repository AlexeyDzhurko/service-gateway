<?php

use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function (Router $router) {
    /**
     * Authentications routes
     */
    $router->group(['prefix' => 'auth'], function (Router $router) {
        $router->post('sign-in', ['as' => 'login', 'uses' => 'AuthController@signIn']);
        $router->post('refresh-token', ['as' => 'refreshToken', 'uses' => 'AuthController@refreshToken']);
    });

    $router->group(['middleware' => 'authGate'], function (Router $router) {
        $router->group(['prefix' => 'users'], function (Router $router) {
            $router->get('/', 'UserController@index');
            $router->get('/{id}', 'UserController@view');
            $router->put('/{id}', 'UserController@update');
            $router->post('/', 'UserController@store');
            $router->delete('/{id}', 'UserController@delete');

            $router->get('/{id}/permissions', 'PermissionsController@getListByUser');

            $router->group(['prefix' => 'permissions'], function (Router $router) {
            });
        });

        $router->group(['prefix' => 'services'], function (Router $router) {
            $router->get('/', 'ServiceController@index');
            $router->get('/{id}', 'ServiceController@view');
            $router->put('/{id}', 'ServiceController@update');
            $router->post('/', 'ServiceController@store');
            $router->delete('/{id}', 'ServiceController@delete');
        });


        $router->group(['prefix' => 'comment'], function () use ($router) {
            $controller = 'ServiceController@send';

            $router->get('/{route:.*}/', $controller);
            $router->post('/{route:.*}/', $controller);
            $router->put('/{route:.*}/', $controller);
            $router->patch('/{route:.*}/', $controller);
            $router->delete('/{route:.*}/', $controller);
        });
    });

    $router->group(['middleware' => 'check-service'], function () use ($router) {

        $controller = 'GateController@sendRequest';

        $router->get('/{route:.*}/', $controller);
        $router->post('/{route:.*}/', $controller);
        $router->put('/{route:.*}/', $controller);
        $router->patch('/{route:.*}/', $controller);
        $router->delete('/{route:.*}/', $controller);
    });
});
