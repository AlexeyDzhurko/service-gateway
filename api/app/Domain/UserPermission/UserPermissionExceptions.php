<?php

declare(strict_types=1);

namespace App\Domain\UserPermission;

use Exception;

/**
 * Class UserPermissionExceptions
 * @package App\Domain\UserPermission
 */
class UserPermissionExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws UserPermissionExceptions
     */
    public static function notFound(int $id)
    {
        throw new UserPermissionExceptions(sprintf('UserPermission with id %d not found', $id));
    }
}
