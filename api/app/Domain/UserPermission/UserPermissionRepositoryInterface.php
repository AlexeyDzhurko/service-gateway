<?php

declare(strict_types=1);

namespace App\Domain\UserPermission;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface UserPermissionRepositoryInterface extends DatabaseRepositoryInterface
{
}
