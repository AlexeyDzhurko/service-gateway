<?php

declare(strict_types=1);

namespace App\Domain\UserPermission;

use App\Contract\Core\FilterInterface;
use App\Domain\Service\Service;
use App\Domain\User\User;
use Illuminate\Http\Request;

/**
 * Class UserPermissionFilter
 * @package App\Domain\UserPernission
 */
class UserPermissionFilter implements FilterInterface
{
    /** @var User|null $user */
    private ?User $user = null;

    /** @var Service|null $service */
    private ?Service $service = null;

    /**
     * @param Request $request
     *
     * @return UserPermissionFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Service|null
     */
    public function getService(): ?Service
    {
        return $this->service;
    }

    /**
     * @param Service|null $service
     */
    public function setService(?Service $service): void
    {
        $this->service = $service;
    }
}
