<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Service\Service;
use App\Domain\UserComment\UserComment;
use App\Domain\UserPermission\UserPermission;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Lumen\Auth\Authorizable;
use Throwable;

/**
 * Class User
 * @package App\Domain\User
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'is_admin'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @return HasMany
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(UserPermission::class);
    }

    /**
     * @return HasManyThrough
     */
    public function services(): HasManyThrough
    {
        return $this->hasManyThrough(
            Service::class,
            UserPermission::class,
            'user_id',
            'id',
            'id',
            'service_id',
        );
    }

    /**
     * @return HasManyThrough
     */
    public function accessedServices(): HasManyThrough
    {
        return $this->hasManyThrough(
            Service::class,
            UserPermission::class,
            'user_id',
            'id',
            'id',
            'service_id',
        )->where('access', true);
    }

    /**
     * THIS IS READONLY RELATION!!
     * @return HasOne|null
     */
    public function comments(): ?HasOne
    {
        try {
            return $this->hasOne(UserComment::class);
        } catch (Throwable $throwable) {
            return null;
        }
    }
}
