<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface UserRepositoryInterface extends DatabaseRepositoryInterface
{
}
