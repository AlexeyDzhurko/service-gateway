<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class UserFilter
 * @package App\Domain\User
 */
class UserFilter implements FilterInterface
{
    /** @var integer|null $id */
    private $id;

    /** @var string|null $email */
    private $email;

    /**
     * @param Request $request
     * @return UserFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setEmail($request->get('email'));

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
