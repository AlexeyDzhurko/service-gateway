<?php

declare(strict_types=1);

namespace App\Domain\UserComment;

use App\Domain\UserPermission\UserPermission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Service
 * @package App\Domain\Service
 */
class UserComment extends Model
{
    /** @var string $connection */
    protected $connection = 'readonly';

    /**
     * @var string[]
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
