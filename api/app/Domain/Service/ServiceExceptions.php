<?php

declare(strict_types=1);

namespace App\Domain\Service;

use Exception;

/**
 * Class ServiceExceptions
 * @package App\Domain\Service
 */
class ServiceExceptions extends Exception
{
    /**
     * @param int $id
     * @throws ServiceExceptions
     */
    public static function notFound(int $id)
    {
        throw new ServiceExceptions(sprintf('Service with id %d not found', $id));
    }
}
