<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface ServiceRepositoryInterface extends DatabaseRepositoryInterface
{
}
