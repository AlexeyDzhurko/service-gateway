<?php

declare(strict_types=1);

namespace App\Contract\Services;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface GateHttpServiceInterface
 * @package App\Contract\Services
 */
interface GateHttpServiceInterface
{
    /**
     * @param string $method
     *
     * @return GateHttpServiceInterface
     */
    public function setMethod(string $method): GateHttpServiceInterface;

    /**
     * @param string $url
     *
     * @return GateHttpServiceInterface
     */
    public function setUrl(string $url): GateHttpServiceInterface;

    /**
     * @param array $queryParams
     *
     * @return GateHttpServiceInterface
     */
    public function setQuery(array $queryParams): GateHttpServiceInterface;

    /**
     * @param array $queryParams
     *
     * @return GateHttpServiceInterface
     */
    public function setBody(array $data): GateHttpServiceInterface;

    /**
     * @param array $queryParams
     *
     * @return GateHttpServiceInterface
     */
    public function setHeaders(array $headers): GateHttpServiceInterface;

    /**
     * @param array $queryParams
     *
     * @return GateHttpServiceInterface
     */
    public function setFiles(array $files): GateHttpServiceInterface;

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function send(): mixed;
}