<?php

declare(strict_types=1);

namespace App\Application\User\GetUsersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetUsersListHandler
 * @package App\Application\User\GetUsersList
 */
class GetUsersListHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * GetUsersListHandler constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUsersList|CommandInterface $command
     *
     * @return LengthAwarePaginator
     */
    public function handle(CommandInterface $command): LengthAwarePaginator
    {
        return $this->userRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting(),
        );
    }
}
