<?php

declare(strict_types=1);

namespace App\Application\User\GetUsersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\User\UserFilter;

/**
 * Class GetUsersList
 * @package App\Application\User\GetUsersList
 */
class GetUsersList implements CommandInterface
{
    /** @var UserFilter $filter */
    private UserFilter $filter;

    /** @var PaginationInterface $pagination */
    private PaginationInterface $pagination;

    /** @var SortingInterface $sorting */
    private SortingInterface $sorting;

    /**
     * GetUsersList constructor.
     * @param UserFilter $filter
     * @param PaginationInterface $pagination
     * @param SortingInterface $sorting
     */
    public function __construct(
        UserFilter $filter,
        PaginationInterface $pagination,
        SortingInterface $sorting
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface
     */
    public function getSorting(): SortingInterface
    {
        return $this->sorting;
    }
}
