<?php

declare(strict_types=1);

namespace App\Application\User\DeleteUser;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class DeleteUserHandler
 * @package App\Application\User\DeleteUser
 */
class DeleteUserHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param CommandInterface|DeleteUser $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $this->userRepository->delete($command->getUser());
    }
}
