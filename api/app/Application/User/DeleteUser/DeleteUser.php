<?php

declare(strict_types=1);

namespace App\Application\User\DeleteUser;

use App\Contract\Core\CommandInterface;
use App\Domain\User\User;

/**
 * Class DeleteUser
 * @package App\Application\User\DeleteUser
 */
class DeleteUser implements CommandInterface
{
    /** @var User $user */
    private User $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
