<?php

declare(strict_types=1);

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class GetUserByFilterHandler
 * @package App\Application\User\GetUserByFilter
 */
class GetUserByFilterHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * GetUserByFilterHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserByFilter|CommandInterface $command
     * @return User
     */
    public function handle(CommandInterface $command): User
    {
        return $this->userRepository->one($command->getFilter());
    }
}
