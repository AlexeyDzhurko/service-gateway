<?php

declare(strict_types=1);

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\User\UserFilter;

/**
 * Class GetUserByFilter
 * @package App\Application\User\GetUserByFilter
 */
class GetUserByFilter implements CommandInterface
{
    /** @var UserFilter $filter */
    private UserFilter $filter;

    /**
     * GetUserByFilter constructor.
     * @param UserFilter $filter
     */
    public function __construct(UserFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }
}
