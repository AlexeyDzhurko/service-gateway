<?php

declare(strict_types=1);

namespace App\Application\User\StoreUser;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

/**
 * Class StoreUserHandler
 * @package App\Application\User\StoreUser
 */
class StoreUserHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * StoreUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param CommandInterface|StoreUser $command
     * @return User
     */
    public function handle(CommandInterface $command): User
    {
        $user = new User([
            'email' => $command->getEmail(),
            'name' => $command->getName(),
            'is_admin' => $command->getIsAdmin(),
        ]);
        $user->password = Hash::make($command->getEmail());

        $this->userRepository->store($user);

        return $user;
    }
}
