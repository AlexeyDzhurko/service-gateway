<?php

declare(strict_types=1);

namespace App\Application\User\StoreUser;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreUser
 * @package App\Application\User\StoreUser
 */
class StoreUser implements CommandInterface
{
    /** @var string $email */
    private string $email;

    /** @var string $name */
    private string $name;

    /** @var boolean $name */
    private bool $isAdmin;

    /**
     * @param string $email
     * @param string $name
     * @param bool $isAdmin
     */
    public function __construct(string $email, string $name, bool $isAdmin)
    {
        $this->email = $email;
        $this->name = $name;
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }
}
