<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUser;

use App\Contract\Core\CommandInterface;
use App\Domain\User\User;

/**
 * Class UpdateUser
 * @package App\Application\User\UpdateUser
 */
class UpdateUser implements CommandInterface
{
    /** @var User $user */
    private User $user;

    /** @var string $email */
    private string $email;

    /** @var string $name */
    private string $name;

    /** @var boolean $name */
    private bool $isAdmin;

    /**
     * @param User $user
     * @param string $email
     * @param string $name
     * @param bool $isAdmin
     */
    public function __construct(User $user, string $email, string $name, bool $isAdmin)
    {
        $this->user = $user;
        $this->email = $email;
        $this->name = $name;
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }
}
