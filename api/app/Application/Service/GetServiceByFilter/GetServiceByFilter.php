<?php

declare(strict_types=1);

namespace App\Application\Service\GetServiceByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Service\ServiceFilter;

/**
 * Class GetServiceByFilter
 * @package App\Application\Service\GetServiceByFilter
 */
class GetServiceByFilter implements CommandInterface
{
    /** @var ServiceFilter $filter */
    private ServiceFilter $filter;

    /**
     * GetServiceByFilter constructor.
     * @param ServiceFilter $filter
     */
    public function __construct(ServiceFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ServiceFilter
     */
    public function getFilter(): ServiceFilter
    {
        return $this->filter;
    }
}
