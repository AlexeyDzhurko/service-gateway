<?php

declare(strict_types=1);

namespace App\Application\Service\GetServiceByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Service\ServiceRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetServiceByFilterHandler
 * @package App\Application\Service\GetServiceByFilter
 */
class GetServiceByFilterHandler implements HandlerInterface
{
    /** @var ServiceRepositoryInterface $serviceRepository */
    private ServiceRepositoryInterface $serviceRepository;

    /**
     * GetServiceByFilterHandler constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param GetServiceByFilter|CommandInterface $command
     * @return Model
     */
    public function handle(CommandInterface $command): Model
    {
        return $this->serviceRepository->one($command->getFilter());
    }
}
