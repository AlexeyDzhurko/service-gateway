<?php

declare(strict_types=1);

namespace App\Application\Service\StoreService;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Service\Service;
use App\Domain\Service\ServiceRepositoryInterface;

/**
 * Class StoreServiceHandler
 * @package App\Application\Service\StoreService
 */
class StoreServiceHandler implements HandlerInterface
{
    /** @var ServiceRepositoryInterface $serviceRepository */
    private $serviceRepository;

    /**
     * StoreServiceHandler constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param CommandInterface|StoreService $command
     * @return Service
     */
    public function handle(CommandInterface $command): Service
    {
        $service = new Service([
            'title' => $command->getTitle(),
            'route' => $command->getRoute(),
            'link' => $command->getLink(),
        ]);

        $this->serviceRepository->store($service);

        return $service;
    }
}
