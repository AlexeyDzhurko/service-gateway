<?php

declare(strict_types=1);

namespace App\Application\Service\StoreService;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreService
 * @package App\Application\Service\StoreService
 */
class StoreService implements CommandInterface
{
    /** @var string $title */
    private string $title;

    /** @var string $route */
    private string $route;

    /** @var string $link */
    private string $link;

    /**
     * @param string $title
     * @param string $route
     * @param string $link
     */
    public function __construct(
        string $title,
        string $route,
        string $link
    ) {
        $this->title = $title;
        $this->route = $route;
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }
}
