<?php

declare(strict_types=1);

namespace App\Application\Service\UpdateService;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Service\Service;
use App\Domain\Service\ServiceRepositoryInterface;

/**
 * Class UpdateServiceHandler
 * @package App\Application\Service\UpdateService
 */
class UpdateServiceHandler implements HandlerInterface
{
    /** @var ServiceRepositoryInterface $serviceRepository */
    private ServiceRepositoryInterface $serviceRepository;

    /**
     * UpdateServiceHandler constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param CommandInterface|UpdateService $command
     * @return Service
     */
    public function handle(CommandInterface $command)
    {
        /** @var Service $service */
        $service = $command->getService();

        $service->title = $command->getTitle();
        $service->route = $command->getRoute();
        $service->link = $command->getLink();

        $this->serviceRepository->store($service);

        return $service;
    }
}
