<?php

declare(strict_types=1);

namespace App\Application\Service\UpdateService;

use App\Contract\Core\CommandInterface;
use App\Domain\Service\Service;

/**
 * Class UpdateService
 * @package App\Application\Service\UpdateService
 */
class UpdateService implements CommandInterface
{
    /** @var Service $Service */
    private Service $Service;

    /** @var string $title */
    private string $title;

    /** @var string $route */
    private string $route;

    /** @var string $link */
    private string $link;

    /**
     * @param Service $Service
     * @param string $title
     * @param string $route
     * @param string $link
     */
    public function __construct(
        Service $Service,
        string $title,
        string $route,
        string $link
    ) {
        $this->Service = $Service;
        $this->title = $title;
        $this->route = $route;
        $this->link = $link;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->Service;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }
}