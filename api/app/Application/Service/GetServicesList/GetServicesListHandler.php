<?php

declare(strict_types=1);

namespace App\Application\Service\GetServicesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Service\ServiceRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetServicesListHandler
 * @package App\Application\Service\GetServicesList
 */
class GetServicesListHandler implements HandlerInterface
{
    /** @var ServiceRepositoryInterface $serviceRepository */
    private ServiceRepositoryInterface $serviceRepository;

    /**
     * GetServicesListHandler constructor.
     *
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param GetServicesList|CommandInterface $command
     *
     * @return LengthAwarePaginator
     */
    public function handle(CommandInterface $command): LengthAwarePaginator
    {
        return $this->serviceRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting(),
        );
    }
}
