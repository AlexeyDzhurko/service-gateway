<?php

declare(strict_types=1);

namespace App\Application\Service\GetServicesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Service\ServiceFilter;

/**
 * Class GetServicesList
 * @package App\Application\Service\GetServicesList
 */
class GetServicesList implements CommandInterface
{
    /** @var ServiceFilter $filter */
    private ServiceFilter $filter;

    /** @var PaginationInterface $pagination */
    private PaginationInterface $pagination;

    /** @var SortingInterface $sorting */
    private SortingInterface $sorting;

    /**
     * GetServicesList constructor.
     * @param ServiceFilter $filter
     * @param PaginationInterface $pagination
     * @param SortingInterface $sorting
     */
    public function __construct(
        ServiceFilter $filter,
        PaginationInterface $pagination,
        SortingInterface $sorting
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return ServiceFilter
     */
    public function getFilter(): ServiceFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface
     */
    public function getSorting(): SortingInterface
    {
        return $this->sorting;
    }
}
