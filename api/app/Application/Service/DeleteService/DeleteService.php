<?php

declare(strict_types=1);

namespace App\Application\Service\DeleteService;

use App\Contract\Core\CommandInterface;
use App\Domain\Service\Service;

/**
 * Class DeleteService
 * @package App\Application\Service\DeleteService
 */
class DeleteService implements CommandInterface
{
    /** @var Service $service */
    private Service $service;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->Service = $service;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }
}
