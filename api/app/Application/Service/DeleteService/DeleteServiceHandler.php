<?php

declare(strict_types=1);

namespace App\Application\Service\DeleteService;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Service\ServiceRepositoryInterface;

/**
 * Class DeleteServiceHandler
 * @package App\Application\Service\DeleteService
 */
class DeleteServiceHandler implements HandlerInterface
{
    /** @var ServiceRepositoryInterface $serviceRepository */
    private ServiceRepositoryInterface $serviceRepository;

    /**
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param CommandInterface|DeleteService $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $this->serviceRepository->delete($command->getService());
    }
}
