<?php

declare(strict_types=1);

namespace App\Application\Gate\SendRequest;

use App\Contract\Core\CommandInterface;
use App\Domain\Service\Service;

/**
 * Class SendRequest
 */
class SendRequest implements CommandInterface
{
    /** @var string $method */
    private string $method;

    /** @var string $path */
    private string $path;

    /** @var array $headers */
    private array $headers;

    /** @var array|null $query */
    private ?array $query;

    /** @var array|null $body */
    private ?array $body;

    /** @var array|null $files */
    private ?array $files;

    /**
     * @param string $method
     * @param string $path
     * @param array $headers
     * @param array|null $query
     * @param array|null $body
     * @param array|null $files
     */
    public function __construct(
        string $method,
        string $path,
        array $headers,
        ?array $query = [],
        ?array $body = [],
        ?array $files = []
    ) {
        $this->method = $method;
        $this->path = $path;
        $this->headers = $headers;
        $this->query = $query;
        $this->body = $body;
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array|null
     */
    public function getQuery(): ?array
    {
        return $this->query;
    }

    /**
     * @return array|null
     */
    public function getBody(): ?array
    {
        return $this->body;
    }

    /**
     * @return array|null
     */
    public function getFiles(): ?array
    {
        return $this->files;
    }
}