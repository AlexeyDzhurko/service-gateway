<?php

declare(strict_types=1);

namespace App\Application\Gate\SendRequest;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Contract\Services\GateHttpServiceInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class SendRequestHandler
 */
class SendRequestHandler implements HandlerInterface
{
    /** @var GateHttpServiceInterface $gateHttpService */
    private GateHttpServiceInterface $gateHttpService;

    /**
     * @param GateHttpServiceInterface $gateHttpService
     */
    public function __construct(GateHttpServiceInterface $gateHttpService)
    {
        $this->gateHttpService = $gateHttpService;
    }

    /**
     * @param CommandInterface $command
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function handle(CommandInterface $command): mixed
    {
        return $this->gateHttpService
            ->setMethod($command->getMethod())
            ->setUrl($command->getPath())
            ->setHeaders($command->getHeaders())
            ->setQuery($command->getQuery())
            ->setBody($command->getBody())
            ->send();
    }
}