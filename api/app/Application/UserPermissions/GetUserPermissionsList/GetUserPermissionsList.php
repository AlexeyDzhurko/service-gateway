<?php

declare(strict_types=1);

namespace App\Application\UserPermissions\GetUserPermissionsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\UserPermission\UserPermissionFilter;

/**
 * Class GetUserPermissionsList
 */
class GetUserPermissionsList implements CommandInterface
{
    /** @var UserPermissionFilter $filter */
    private UserPermissionFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * @param UserPermissionFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        UserPermissionFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return UserPermissionFilter
     */
    public function getFilter(): UserPermissionFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}