<?php

declare(strict_types=1);

namespace App\Application\UserPermissions\GetUserPermissionsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\UserPermission\UserPermissionRepositoryInterface;

/**
 * Class GetUserPermissionsListHandler
 * @package App\Application\UserPermissions\GetUserPermissionsList
 */
class GetUserPermissionsListHandler implements HandlerInterface
{
    /** @var UserPermissionRepositoryInterface $userPermissionRepository */
    private UserPermissionRepositoryInterface $userPermissionRepository;

    /**
     * @param UserPermissionRepositoryInterface $userPermissionRepository
     */
    public function __construct(UserPermissionRepositoryInterface $userPermissionRepository)
    {
        $this->userPermissionRepository = $userPermissionRepository;
    }

    /**
     * @param CommandInterface|GetUserPermissionsList $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        return $this->userPermissionRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting(),
        );
    }
}