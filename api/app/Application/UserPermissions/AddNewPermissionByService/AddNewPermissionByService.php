<?php

declare(strict_types=1);

namespace App\Application\UserPermissions\AddNewPermissionByService;

use App\Contract\Core\CommandInterface;
use App\Domain\Service\Service;

/**
 * Class AddNewPermissionByService
 * @package App\Application\UserPermissions\AddNewPermissionByService
 */
class AddNewPermissionByService implements CommandInterface
{
    /** @var Service $service */
    private Service $service;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }
}
