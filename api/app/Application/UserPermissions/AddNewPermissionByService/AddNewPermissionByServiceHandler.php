<?php

declare(strict_types=1);

namespace App\Application\UserPermissions\AddNewPermissionByService;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use App\Domain\UserPermission\UserPermission;
use App\Domain\UserPermission\UserPermissionRepositoryInterface;
use App\Infrastructure\Core\Pagination;

/**
 * Class AddNewPermissionByServiceHandler
 * @package App\Application\UserPermissions\AddNewPermissionByService
 */
class AddNewPermissionByServiceHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /** @var UserPermissionRepositoryInterface $userPermissionRepository */
    private UserPermissionRepositoryInterface $userPermissionRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param UserPermissionRepositoryInterface $userPermissionRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserPermissionRepositoryInterface $userPermissionRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userPermissionRepository = $userPermissionRepository;
    }

    /**
     * @param AddNewPermissionByService|CommandInterface $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $pagination = new Pagination(1, 2);
        $filter = new UserFilter();
        $isLastPage = false;

        do {
            $res = $this->userRepository->all($filter, $pagination);
            foreach ($res->items() as $user) {
                $userPermission = new UserPermission();
                $userPermission->service()->associate($command->getService());
                $userPermission->user()->associate($user);
                $userPermission->access = $user->is_admin;

                $this->userPermissionRepository->store($userPermission);
            }

            $pagination->setPage($pagination->getPage() + 1);
            $isLastPage = (bool)($res->currentPage() == $res->lastPage());
        } while (!$isLastPage);
    }
}