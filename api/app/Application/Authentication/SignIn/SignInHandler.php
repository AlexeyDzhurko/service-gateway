<?php

declare(strict_types=1);

namespace App\Application\Authentication\SignIn;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Contract\Services\JwtServiceInterface;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class SignInHandler
 * @package App\Commands\Authentication\SignIn
 */
class SignInHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /** @var JwtServiceInterface $jwtService */
    private JwtServiceInterface $jwtService;

    /**
     * SignInHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param JwtServiceInterface $jwtService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        JwtServiceInterface $jwtService
    ) {
        $this->userRepository = $userRepository;
        $this->jwtService = $jwtService;
    }

    /**
     * @param SignIn|CommandInterface $command
     * @return array
     *
     * @throw UnauthorizedException
     * @throw Exception
     */
    public function handle(CommandInterface $command): array
    {
        $filter = new UserFilter();
        $filter->setEmail($command->getEmail());

        $user = $this->userRepository->one($filter);

        if (!$user) {
            throw new UnauthorizedException('Email or password is wrong');
        }

        if (!Hash::check($command->getPassword(), $user->password)) {
            throw new UnauthorizedException('Email or password is wrong');
        }

        $user->last_login_at = Carbon::now();

        return $this->jwtService->generateTokenToUser($user);
    }
}
