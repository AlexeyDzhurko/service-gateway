<?php

declare(strict_types=1);

namespace App\Application\Authentication\SignIn;

use App\Contract\Core\CommandInterface;

/**
 * Class SignIn
 * @package App\Commands\Authentication\SignIn
 */
class SignIn implements CommandInterface
{
    /** @var string $email */
    private string $email;

    /** @var string $password */
    private string$password;

    /**
     * SignIn constructor.
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
