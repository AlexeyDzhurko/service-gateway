<?php

declare(strict_types=1);

namespace App\Application\Authentication\RestoreToken;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Contract\Services\JwtServiceInterface;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class RestoreTokenHandler
 * @package App\Commands\Authentication\RestoreToken
 */
class RestoreTokenHandler implements HandlerInterface
{
    /** @var JwtServiceInterface $jwtService */
    private JwtServiceInterface $jwtService;

    /**
     * RestoreTokenHandler constructor.
     * @param JwtServiceInterface $jwtService
     */
    public function __construct(JwtServiceInterface $jwtService)
    {
        $this->jwtService = $jwtService;
    }

    /**
     * @param RestoreToken|CommandInterface $command
     * @return array
     *
     * @throws UnauthorizedException
     */
    public function handle(CommandInterface $command): array
    {
        return $this->jwtService->refreshToken($command->getOldToken());
    }
}
