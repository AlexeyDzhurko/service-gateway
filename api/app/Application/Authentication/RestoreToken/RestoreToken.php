<?php

declare(strict_types=1);

namespace App\Application\Authentication\RestoreToken;

use App\Contract\Core\CommandInterface;

/**
 * Class RestoreToken
 * @package App\Commands\Authentication\RestoreToken
 */
class RestoreToken implements CommandInterface
{
    /** @var string $oldToken */
    private string $oldToken;

    /**
     * RestoreToken constructor.
     * @param string $oldToken
     */
    public function __construct(string $oldToken)
    {
        $this->oldToken = $oldToken;
    }

    /**
     * @return string
     */
    public function getOldToken(): string
    {
        return $this->oldToken;
    }
}
