<?php

declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Contract\Services\GateHttpServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class GateHttpService
 * @package App\Infrastructure\Services
 */
class GateHttpService implements GateHttpServiceInterface
{
    /** @var Client $client */
    private Client $client;

    /** @var string|null $method */
    private ?string $method = null;

    /** @var string|null $url */
    private ?string $url = null;

    /** @var array|null $headers */
    private ?array $headers = null;

    /** @var array|null $query */
    private ?array $query = null;

    /** @var array|null $body */
    private ?array $body = null;

    /** @var array|null $files */
    private ?array $files = null;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     *
     * @return GateHttpServiceInterface
     */
    public function setMethod(string $method): GateHttpServiceInterface
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param string $url
     *
     * @return GateHttpServiceInterface
     */
    public function setUrl(string $url): GateHttpServiceInterface
    {
        $this->url = $url;

        return $this;
    }


    /**
     * @param array $queryParams
     *
     * @return GateHttpServiceInterface
     */
    public function setQuery(array $queryParams): GateHttpServiceInterface
    {
        $this->query = $queryParams;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return GateHttpServiceInterface
     */
    public function setBody(array $data): GateHttpServiceInterface
    {
        $this->body = $data;

        return $this;
    }

    /**
     * @param array $headers
     *
     * @return GateHttpServiceInterface
     */
    public function setHeaders(array $headers): GateHttpServiceInterface
    {
        $data = [];

        foreach ($headers as $key => $val) {
            $data[$key] = head($val);
        }

        $this->headers = $data;

        return $this;
    }

    /**
     * @param array $files
     *
     * @return GateHttpServiceInterface
     */
    public function setFiles(array $files): GateHttpServiceInterface
    {
        // TODO: Implement setFiles() method.
    }

    /**
     * @return mixed
     * @throws GuzzleException
     * @throws Throwable
     */
    public function send(): mixed
    {
        try {
            $response = $this->client->request($this->method, new Uri($this->url),
                [
                    RequestOptions::HEADERS => $this->headers,
                    RequestOptions::QUERY => $this->query,
                    RequestOptions::FORM_PARAMS => $this->body,
                ]
            );

            return $response->getBody()->getContents();
        } catch (ClientException $exception) {
            Log::error($exception->getMessage(), $exception->getTrace());
            throw $exception;
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage(), $throwable->getTrace());
            throw $throwable;
        }
    }
}