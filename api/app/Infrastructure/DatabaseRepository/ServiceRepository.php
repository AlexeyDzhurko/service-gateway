<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Service\Service;
use App\Domain\Service\ServiceFilter;
use App\Domain\Service\ServiceRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ServiceRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class ServiceRepository extends DatabaseRepository implements ServiceRepositoryInterface
{
    /**
     * ServiceRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Service();
    }

    /**
     * @param FilterInterface|ServiceFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
