<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\UserPermission\UserPermission;
use App\Domain\UserPermission\UserPermissionRepositoryInterface;
use App\Domain\UserPermission\UserPermissionFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPermissionRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class UserPermissionRepository extends DatabaseRepository implements UserPermissionRepositoryInterface
{
    /**
     * UserPermissionRepository constructor.
     */
    public function __construct()
    {
        $this->model = new UserPermission();
    }

    /**
     * @param FilterInterface|UserPermissionFilter $filter
     *
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($user = $filter->getUser()) {
            $this->builder->where('user_id', $user->id);
        }

        if ($service = $filter->getService()) {
            $this->builder->where('service_id', $service->id);
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
