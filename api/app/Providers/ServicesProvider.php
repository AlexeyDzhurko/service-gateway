<?php

declare(strict_types=1);

namespace App\Providers;

use App\Contract\Services\GateHttpServiceInterface;
use App\Contract\Services\JwtServiceInterface;
use App\Infrastructure\Services\GateHttpService;
use App\Infrastructure\Services\JwtService;
use Illuminate\Support\ServiceProvider;

/**
 * Class ServicesProvider
 * @package App\Providers
 */
class ServicesProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(JwtServiceInterface::class, JwtService::class);
        $this->app->singleton(GateHttpServiceInterface::class, GateHttpService::class);
    }
}
