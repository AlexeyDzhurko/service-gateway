<?php

declare(strict_types=1);

namespace App\Providers;

use App\Domain\Service\ServiceRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use App\Domain\UserPermission\UserPermissionRepositoryInterface;
use App\Infrastructure\DatabaseRepository\ServiceRepository;
use App\Infrastructure\DatabaseRepository\UserPermissionRepository;
use App\Infrastructure\DatabaseRepository\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class DomainServiceProvider
 * @package App\Providers
 */
class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->singleton(
            UserPermissionRepositoryInterface::class,
            UserPermissionRepository::class
        );
        $this->app->singleton(
            ServiceRepositoryInterface::class,
            ServiceRepository::class
        );
    }
}
