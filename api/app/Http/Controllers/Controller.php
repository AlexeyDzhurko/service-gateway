<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contract\Core\CommandBusInterface;
use App\Contract\Core\CommandInterface;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /** @var CommandBusInterface $dispatcher */
    protected $dispatcher;

    /**
     * Controller constructor.
     * @param CommandBusInterface $dispatcher
     */
    public function __construct(CommandBusInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param CommandInterface $command
     * @return mixed
     */
    public function execute(CommandInterface $command)
    {
        return $this->dispatcher->dispatch($command);
    }
}
