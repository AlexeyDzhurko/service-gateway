<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Application\Gate\SendRequest\SendRequest;
use Illuminate\Http\Request;

/**
 * Class GateController
 * @package App\Http\Controllers
 */
class GateController extends Controller
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function sendRequest(Request $request): mixed
    {
        $service = $request->service;
        $url = sprintf('%s/%s', $service->link, $request->path());

        $res = $this->execute(new SendRequest(
            $request->method(),
            $url,
            $request->headers->all(),
            $request->query(),
            $request->all(),
            $request->allFiles(),
        ));

        return $res;
    }
}