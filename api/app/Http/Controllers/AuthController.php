<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Application\Authentication\RestoreToken\RestoreToken;
use App\Application\Authentication\SignIn\SignIn;
use App\Http\Requests\Dashboard\Auth\LoginRequest;
use App\Http\Resourses\Auth\AuthDataResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers\Dashboard
 */
class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     *
     * @return JsonResponse
     */
    public function signIn(LoginRequest $request): JsonResponse
    {
        $token = $this->execute(new SignIn(
                $request->get('email'),
                $request->get('password')
            )
        );

        return new JsonResponse(new AuthDataResource($token));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function refreshToken(Request $request): JsonResponse
    {
        $tokenData = $this->execute(new RestoreToken($request->header('Authorization')));

        return new JsonResponse(new AuthDataResource($tokenData));
    }
}
