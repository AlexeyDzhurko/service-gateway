<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Application\User\DeleteUser\DeleteUser;
use App\Application\User\GetUserByFilter\GetUserByFilter;
use App\Application\User\GetUsersList\GetUsersList;
use App\Application\User\StoreUser\StoreUser;
use App\Application\User\UpdateUser\UpdateUser;
use App\Domain\User\UserFilter;
use App\Http\Requests\Dashboard\User\CreateRequest;
use App\Http\Requests\Dashboard\User\UpdateRequest;
use App\Http\Resourses\User\UserResource;
use App\Http\Resourses\User\UsersListResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\Dashboard
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $list = $this->execute(new GetUsersList(
            UserFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request)
        ));

        return new JsonResponse(new UsersListResource($list));
    }

    /**
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function view($id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId((int)$id);

        $user = $this->execute(new GetUserByFilter($filter));

        return new JsonResponse(new UserResource($user));
    }

    /**
     * @param CreateRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateRequest $request): JsonResponse
    {
        $user = $this->execute(new StoreUser(
            $request->get('email'),
            $request->get('name'),
            $request->get('isAdmin', false)
        ));

        return new JsonResponse(new UserResource($user));
    }

    /**
     * @param UpdateRequest $request
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, $id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId((int)$id);

        $user = $this->execute(new GetUserByFilter($filter));
        $user = $this->execute(new UpdateUser(
            $user,
            $request->get('email'),
            $request->get('name'),
            $request->get('isAdmin', false)
        ));

        return new JsonResponse(new UserResource($user));
    }

    /**
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId((int)$id);

        $user = $this->execute(new GetUserByFilter($filter));
        $this->execute(new DeleteUser($user));

        return new JsonResponse([], 204);
    }
}
