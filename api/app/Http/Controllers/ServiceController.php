<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Application\Service\GetServicesList\GetServicesList;
use App\Application\Service\DeleteService\DeleteService;
use App\Application\Service\GetServiceByFilter\GetServiceByFilter;
use App\Application\Service\StoreService\StoreService;
use App\Application\Service\UpdateService\UpdateService;
use App\Application\UserPermissions\AddNewPermissionByService\AddNewPermissionByService;
use App\Domain\Service\ServiceFilter;
use App\Http\Requests\Dashboard\Service\CreateServiceRequest;
use App\Http\Requests\Dashboard\Service\UpdateServiceRequest;
use App\Http\Resourses\Service\ServiceResource;
use App\Http\Resourses\Service\ServicesListResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ServiceController
 * @package App\Http\Controllers
 */
class ServiceController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $list = $this->execute(new GetServicesList(
            ServiceFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request)
        ));

        return new JsonResponse(new ServicesListResource($list));
    }

    /**
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function view($id): JsonResponse
    {
        $filter = new ServiceFilter();
        $filter->setId((integer)$id);

        $Service = $this->execute(new GetServiceByFilter($filter));

        return new JsonResponse(new ServiceResource($Service));
    }

    /**
     * @param CreateServiceRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateServiceRequest $request): JsonResponse
    {
        $service = $this->execute(new StoreService(
            $request->get('title'),
            $request->get('route'),
            $request->get('link')
        ));
        $this->execute(new AddNewPermissionByService($service));

        return new JsonResponse(new ServiceResource($service));
    }

    /**
     * @param UpdateServiceRequest $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function update(UpdateServiceRequest $request, $id): JsonResponse
    {
        $filter = new ServiceFilter();
        $filter->setId((int)$id);

        $Service = $this->execute(new GetServiceByFilter($filter));
        $Service = $this->execute(new UpdateService(
            $Service,
            $request->get('title'),
            $request->get('route'),
            $request->get('link')
        ));

        return new JsonResponse(new ServiceResource($Service));
    }

    /**
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $filter = new ServiceFilter();
        $filter->setId($id);

        $Service = $this->execute(new GetServiceByFilter($filter));
        $this->execute(new DeleteService($Service));

        return new JsonResponse([], 204);
    }
}