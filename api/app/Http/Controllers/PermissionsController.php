<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Application\User\GetUserByFilter\GetUserByFilter;
use App\Application\UserPermissions\GetUserPermissionsList\GetUserPermissionsList;
use App\Domain\User\UserFilter;
use App\Domain\UserPermission\UserPermissionFilter;
use App\Http\Resourses\UserPermission\UserPermissionsListResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class PermissionsController
 * @package App\Http\Controllers
 */
class PermissionsController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function getListByUser(Request $request, $id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId((int)$id);
        $user = $this->execute(new GetUserByFilter($filter));

        $permissionsFilter = new UserPermissionFilter();
        $permissionsFilter->setUser($user);

        $list = $this->execute(new GetUserPermissionsList(
            $permissionsFilter
        ));

        return new JsonResponse(new UserPermissionsListResource($list));
    }

    public function updateByUser()
    {

    }
}