<?php

declare(strict_types=1);

namespace App\Http\Resourses\UserPermission;

use App\Domain\UserPermission\UserPermission;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Service\ServiceResource;
use App\Http\Resourses\User\UserResource;

/**
 * Class UserPermissionsListResource
 * @package App\Http\Resourses\UserPermission
 */
class UserPermissionsListResource extends BaseCollectionResource
{
    /**
     * @param UserPermission $item
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'type' => 'user-permission',
            'attributes' => [
                'access' => $item->access,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'user' => new UserResource($item->user),
                'service' => new ServiceResource($item->service)
            ]
        ];
    }
}