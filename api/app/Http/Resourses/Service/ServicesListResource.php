<?php

declare(strict_types=1);

namespace App\Http\Resourses\Service;

use App\Domain\Service\Service;
use App\Domain\User\User;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class ServicesListResource
 * @package App\Http\Resourses\User
 */
class ServicesListResource extends BaseCollectionResource
{
    /**
     * @param User $item
     * @return array
     */
    protected function getItemData($item): array
    {
        /** @var Service $item */
        return [
            'id' => $item->id,
            'type' => 'service',
            'attributes' => [
                'title' => $item->title,
                'route' => $item->route,
                'link' => $item->link,
                'createdAt' => $item->created_at->format('Y-m-d H:i:s')
            ]
        ];
    }
}