<?php

declare(strict_types=1);

namespace App\Http\Resourses\Service;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ServiceResource
 * @package App\Http\Resourses\Service
 */
class ServiceResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array|void
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'service',
            'attributes' => [
                'title' => $this->title,
                'route' => $this->route,
                'link' => $this->link,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null
            ]
        ];
    }
}