<?php

declare(strict_types=1);

namespace App\Http\Resourses\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resourses\User
 */
class UserResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array|void
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => 'user',
            'attributes' => [
                'email' => $this->email,
                'name' => $this->name,
                'isAdmin' => $this->is_admin,
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : "",
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : ""
            ]
        ];

        if ($this->comment) {
            $data['relationships']['comments'] = [
                'id' => $this->comment->id,
                'type' => 'user-comment',
                'attributes' => [
                    'text' => $this->comment->text,
                    'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : "",
                    'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : ""
                ]
            ];
        }

        return $data;
    }
}
