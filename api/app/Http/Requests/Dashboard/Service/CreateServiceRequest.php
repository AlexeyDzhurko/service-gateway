<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Service;

use App\Http\Requests\FormRequest;

/**
 * Class CreateServiceRequest
 */
class CreateServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'route' => 'required|string',
            'link' => 'required|string',
        ];
    }
}